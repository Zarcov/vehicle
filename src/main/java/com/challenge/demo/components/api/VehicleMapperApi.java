package com.challenge.demo.components.api;

import com.challenge.demo.entities.models.Vehicle;

import java.util.List;

public interface VehicleMapperApi {

    Vehicle transform(String[] record);

    List<Vehicle> transformAll(List<String[]> record);

}
