package com.challenge.demo.components.impl;

import com.challenge.demo.components.api.VehicleMapperApi;
import com.challenge.demo.entities.models.Vehicle;
import com.challenge.demo.utils.TypeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class VehicleMapper implements VehicleMapperApi {

    @Autowired
    TypeUtils typeUtils;

    @Override
    public Vehicle transform(String[] record) {

        //Invalid Vehicle to map (No Input or Invalid SIze or no Time)
        if(record == null || record.length != 15 || record[0].isEmpty()){return null;};

        Instant time = typeUtils.parseDateToInstant(record[0]);
        Integer lineId = typeUtils.parseStringToInt(record[1]);
        Integer direction = typeUtils.parseStringToInt(record[2]);
        Integer jouneyPatternId = typeUtils.parseStringToInt(record[3]);
        String timeFrame = record[4];
        Integer vehicleJourneyId = typeUtils.parseStringToInt(record[5]);
        String operator = record[6];
        Integer congestion = typeUtils.parseStringToInt(record[7]);
        Double longitude = typeUtils.parseStringToDouble(record[8]);
        Double latitude = typeUtils.parseStringToDouble(record[9]);
        Integer delay = typeUtils.parseStringToInt(record[10]);
        Integer blockId = typeUtils.parseStringToInt(record[11]);
        Integer vehicleId = typeUtils.parseStringToInt(record[12]);
        Integer stopId = typeUtils.parseStringToInt(record[13]);
        boolean stop = typeUtils.parseStringToBoolean(record[14]);

        Vehicle vehicle = Vehicle.builder()
                .time(time)
                .lineId(lineId)
                .direction(direction)
                .jouneyPatternId(jouneyPatternId)
                .timeFrame(timeFrame)
                .vehicleJourneyId(vehicleJourneyId)
                .operator(operator)
                .congestion(congestion)
                .longitude(longitude)
                .latitude(latitude)
                .delay(delay)
                .blockId(blockId)
                .vehicleId(vehicleId)
                .stopId(stopId)
                .stop(stop)
                .build();

        return vehicle;
    }

    @Override
    public List<Vehicle> transformAll(List<String[]> record) {

        List<Vehicle> recordList = record
                .stream()
                .map(r -> this.transform(r))
                .collect(Collectors.toList());

        return recordList;
    }
}
