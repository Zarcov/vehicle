package com.challenge.demo.repositories.impl;

import com.challenge.demo.entities.models.Vehicle;
import com.challenge.demo.repositories.api.VehicleRepositoryApi;
import com.challenge.demo.repositories.template.RepositoryTemplate;
import org.influxdb.InfluxDB;
import org.influxdb.dto.BatchPoints;
import org.influxdb.dto.Point;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.concurrent.TimeUnit;

@Repository
public class VehicleRepository extends RepositoryTemplate<Vehicle> implements VehicleRepositoryApi {

    @Override
    public void insertEntities (List<Vehicle> vehicleList){

        BatchPoints batchPoints = BatchPoints
                .database(dbName)
                .consistency(InfluxDB.ConsistencyLevel.ALL)
                    .build();

            vehicleList.forEach(v -> {

            Point point = Point.measurement(measurementName)
                    .time(v.getTime().toEpochMilli(), TimeUnit.MICROSECONDS)

                    .addField("lineId", v.getLineId())
                    .addField("direction", v.getDirection())
                    .addField("jouneyPatternId", v.getJouneyPatternId())
                    .addField("timeFrame", v.getTimeFrame())
                    .addField("vehicleJourneyId", v.getVehicleJourneyId())
                    .addField("operator", v.getOperator())
                    .addField("congestion", v.getCongestion())
                    .addField("longitude", v.getLongitude())
                    .addField("latitude", v.getLatitude())
                    .addField("delay", v.getDelay())
                    .addField("blockId", v.getBlockId())
                    .addField("vehicleId", v.getVehicleId())
                    .addField("stopId", v.getStopId())
                    .addField("stop", v.isStop())
                    .build();

            batchPoints.point(point);
        });


        dbContext.write(batchPoints);


    }

}
