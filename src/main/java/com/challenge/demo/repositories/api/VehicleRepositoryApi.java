package com.challenge.demo.repositories.api;

import com.challenge.demo.entities.models.Vehicle;

import java.util.List;

public interface VehicleRepositoryApi {

    void insertEntities(List<Vehicle> entityList);


}
