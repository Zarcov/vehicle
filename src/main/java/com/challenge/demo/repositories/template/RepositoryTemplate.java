package com.challenge.demo.repositories.template;

import org.influxdb.InfluxDB;
import org.influxdb.dto.Query;
import org.influxdb.dto.QueryResult;
import org.influxdb.impl.InfluxDBResultMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class RepositoryTemplate<T>   {

    @Value("${server.database.measurementname}")
    protected String measurementName;

    @Autowired
    @Qualifier("dbContext")
    protected InfluxDB dbContext;

    @Value("${server.database.name}")
    protected String dbName;

    public List<T> executeQuery(String  inputQuery, Class<T> classT) {

        Query query = new Query(inputQuery, dbName);

        QueryResult queryResult = dbContext.query(query);
        InfluxDBResultMapper resultMapper = new InfluxDBResultMapper();

        List<T> entityList = resultMapper
                .toPOJO(queryResult, classT);

        return entityList;

    };

}
