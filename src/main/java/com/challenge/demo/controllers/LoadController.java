package com.challenge.demo.controllers;

import com.challenge.demo.entities.request.FileRequest;
import com.challenge.demo.services.impl.LoadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/api")
public class LoadController {

    @Autowired
    private LoadService loadServiceApi;

    @RequestMapping(value = "/vehicles/fileupload", method = RequestMethod.POST)
    public void loadExternalData(@RequestParam("file") MultipartFile file) {

        loadServiceApi.loadExternalData(file);
    }

    @RequestMapping(value = "/vehicles/nonfileupload", method = RequestMethod.POST)
    public void loadExternalData(@RequestBody FileRequest fileRequest) {

        loadServiceApi.loadExternalData(fileRequest.getFileName());
    }
}

