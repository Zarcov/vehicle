package com.challenge.demo.controllers;

import com.challenge.demo.constants.ApplicationConstants;
import com.challenge.demo.entities.errors.ApiError;
import com.challenge.demo.entities.errors.DateInputError;
import com.challenge.demo.entities.errors.FileNotExistsError;
import com.challenge.demo.entities.errors.NotFoundError;
import org.springframework.beans.ConversionNotSupportedException;
import org.springframework.beans.TypeMismatchException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
@RestController
public class ErrorController extends ResponseEntityExceptionHandler {

    @Autowired
    ApplicationConstants applicationConstants;

    @Override
    protected ResponseEntity<Object> handleMissingServletRequestParameter(MissingServletRequestParameterException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

        ApiError apiError = new ApiError();
        apiError.setErrorMessage(ex.toString());
        apiError.setName(applicationConstants.BAD_REQUEST_MISSING_ERROR_TYPE_CODE);
        apiError.setStatus(HttpStatus.BAD_REQUEST);

        ResponseEntity<Object> result = new ResponseEntity<>(apiError, apiError.getStatus());

        return result;
    }

    @Override
    protected ResponseEntity<Object> handleTypeMismatch(TypeMismatchException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

        ApiError apiError = new ApiError();
        apiError.setErrorMessage(ex.toString());
        apiError.setName(applicationConstants.BAD_REQUEST_BAD_TYPE_ERROR_TYPE_CODE);
        apiError.setStatus(HttpStatus.BAD_REQUEST);

        ResponseEntity<Object> result = new ResponseEntity<>(apiError, apiError.getStatus());

        return result;
    }

    @ExceptionHandler(value = { DateInputError.class })
    protected ResponseEntity<Object> handleBadDateFormat(
            DateInputError ex, WebRequest request) {

        ApiError apiError = new ApiError();
        apiError.setErrorMessage(ex.toString());
        apiError.setName(applicationConstants.BAD_REQUEST_DATE_FORMAT_ERROR_TYPE_CODE);
        apiError.setStatus(HttpStatus.BAD_REQUEST);

        return handleExceptionInternal(ex, apiError, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler(value = { FileNotExistsError.class })
    protected ResponseEntity<Object> handleNonExistentFile(
            DateInputError ex, WebRequest request) {

        ApiError apiError = new ApiError();
        apiError.setErrorMessage(ex.toString());
        apiError.setName(applicationConstants.BAD_REQUEST_FILE_NOT_EXISTS_ERROR_TYPE_CODE);
        apiError.setStatus(HttpStatus.BAD_REQUEST);

        return handleExceptionInternal(ex, apiError, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler(value = { NotFoundError.class })
    protected ResponseEntity<Object> handleEntityNotFound(
            NotFoundError ex, WebRequest request) {

        ApiError apiError = new ApiError();
        apiError.setErrorMessage(applicationConstants.NOT_FOUND_ERROR_MESSAGE);
        apiError.setName(applicationConstants.NOT_FOUND_ERROR_TYPE_CODE);
        apiError.setStatus(HttpStatus.NOT_FOUND);

        return handleExceptionInternal(ex, apiError, new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }

    @ExceptionHandler(value = { Exception.class })
    protected ResponseEntity<Object> handleInternalServerError(Exception ex, WebRequest request) {

        ApiError apiError = new ApiError();
        apiError.setErrorMessage(ex.toString());
        apiError.setName(applicationConstants.INTERNAL_SERVER_ERROR_TYPE_CODE);
        apiError.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);

        return handleExceptionInternal(ex, apiError, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
    }

}
