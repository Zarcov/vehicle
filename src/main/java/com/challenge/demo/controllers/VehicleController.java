package com.challenge.demo.controllers;

import com.challenge.demo.entities.dtos.VehicleTraceDTO;
import com.challenge.demo.entities.models.Operator;
import com.challenge.demo.entities.models.SimplifiedVehicle;
import com.challenge.demo.entities.models.VehicleTrace;
import com.challenge.demo.services.api.VehicleServiceApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class VehicleController {

    @Autowired
    private VehicleServiceApi vehicleServiceApi;

    //Endpoint1 => Running Operators have at least 1 record not Stopped (Stop=false) in the Time Interval
    @RequestMapping(value = "/operators/running", method = RequestMethod.GET)
    public ResponseEntity<List<Operator>> getOperatorsRunningByTimeInterval(
            @RequestParam("startTime") String startTime,
            @RequestParam("endTime") String endTime) {

        List<Operator> operatorList = vehicleServiceApi.getOperatorsRunningByTimeInterval(startTime,endTime);

        return ResponseEntity.ok(operatorList);
    }

    //Endpoint2
    @RequestMapping(value = "/vehicles/operator/{operator}", method = RequestMethod.GET)
    public ResponseEntity<List<SimplifiedVehicle>> getVehiclesByOperatorAndTimeInterval(
            @RequestParam("startTime") String startTime,
            @RequestParam("endTime") String endTime,
            @PathVariable("operator") String operator) {

        List<SimplifiedVehicle> sVehicle = vehicleServiceApi.getVehiclesByOperatorAndTimeInterval(startTime,endTime, operator);

        return ResponseEntity.ok(sVehicle);
    }

    //Endpoint3 => Stopped Vehicles have all records saying they are Stopped (Stop=true) in the Time Interval
    @RequestMapping(value = "/vehicles/stop/operator/{operator}", method = RequestMethod.GET)
    public ResponseEntity<List<SimplifiedVehicle>> getVehiclesAtStopByOperatorAndTimeInterval(
            @RequestParam("startTime") String startTime,
            @RequestParam("endTime") String endTime,
            @PathVariable("operator") String operator) {

        List<SimplifiedVehicle> operatorList = vehicleServiceApi.getVehiclesAtStopByOperator(startTime,endTime, operator);

        return ResponseEntity.ok(operatorList);
    }

    //Endpoint4
    @RequestMapping(value = "/trace/vehicle/{vehicleId}", method = RequestMethod.GET)
    public ResponseEntity<List<VehicleTraceDTO>> getTraceByVehicleAndTimeInterval(
            @RequestParam("startTime") String startTime,
            @RequestParam("endTime") String endTime,
            @PathVariable("vehicleId") int vehicleId) {

        List<VehicleTraceDTO> vehicleTrace = vehicleServiceApi.getTraceByVehicle(startTime,endTime, vehicleId);

        return ResponseEntity.ok(vehicleTrace);
    }

}
