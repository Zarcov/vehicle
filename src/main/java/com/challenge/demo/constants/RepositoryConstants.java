package com.challenge.demo.constants;

import org.springframework.stereotype.Component;

@Component
public class RepositoryConstants {

    public String GET_OPERATORS_BY_TIME_INTERVAL_RUNNING = "SELECT DISTINCT(operator) as operator FROM Vehicle WHERE time >= '%1s' AND time <= '%2s' AND stop=false";

    public String GET_VEHICLES_BY_OPERATOR_AND_TIME_INTERVAL = "SELECT DISTINCT(vehicleId) as vehicleId FROM Vehicle WHERE operator='%1s' AND time >= '%2s' AND time <= '%3s'";

    public String GET_VEHICLES_WITH_STOP_BY_OPERATOR_AND_TIME_INTERVAL = "SELECT DISTINCT(vehicleId) as vehicleId FROM Vehicle WHERE operator='%1s' AND time >= '%2s' AND time <= '%3s' AND stop=true";

    public String GET_VEHICLES_RUNNING_BY_OPERATOR_AND_TIME_INTERVAL = "SELECT DISTINCT(vehicleId) as vehicleId FROM Vehicle WHERE operator='%1s' AND time >= '%2s' AND time <= '%3s' AND stop=false";

    public String GET_VEHICLE_TRACE_AND_TIME_INTERVAL = "SELECT time, latitude, longitude FROM Vehicle WHERE vehicleId = %1s AND time >= '%2s' AND time <= '%3s' order by time desc";

}
