package com.challenge.demo.constants;

import org.springframework.context.annotation.Configuration;

@Configuration
public class ApplicationConstants {


    public String TIME_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS";

    //ERRORS 400
    public String BAD_REQUEST_MISSING_ERROR_TYPE_CODE = "MissingParameter";
    public String BAD_REQUEST_BAD_TYPE_ERROR_TYPE_CODE = "TypeMissmatch";
    public String BAD_REQUEST_DATE_FORMAT_ERROR_TYPE_CODE = "BadDateFormat";
    public String BAD_REQUEST_FILE_NOT_EXISTS_ERROR_TYPE_CODE = "FileNotExists";
    public String BAD_REQUEST_DATE_FORMAT_ERROR_MESSAGE = "TimeFormat: '%1s' | StartTime: '%2s' | EndTime: '%3s'";

    //ERRORS 404
    public String NOT_FOUND_ERROR_TYPE_CODE = "NotFound";
    public String NOT_FOUND_ERROR_MESSAGE = "No Result was found";

    //Error 500
    public String INTERNAL_SERVER_ERROR_TYPE_CODE = "InternalServerError";
}
