package com.challenge.demo.services.template;

import com.challenge.demo.validators.api.RequestValidatorApi;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public abstract class ServiceTemplate {

    @Autowired
    private RequestValidatorApi requestValidator;

    public void validateTimeInterval(String startTime, String endTime){

        requestValidator.isInputStringNull("startTime", startTime);
        requestValidator.isInputStringNull("endTime", startTime);
        requestValidator.isTimeIntervalValid(startTime, endTime);

    }

    public String buildQuery (String templateQuery, List<String> parameterList){

       Object[] a = parameterList.toArray();
       return String.format(templateQuery,a);

    }
}
