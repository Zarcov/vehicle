package com.challenge.demo.services.impl;

import com.challenge.demo.services.api.FileStorageApi;
import com.challenge.demo.services.api.LoadServiceApi;
import com.challenge.demo.services.api.VehicleServiceApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.Queue;

@Service
public class LoadService implements LoadServiceApi {

    @Autowired
    private FileStorageApi fileStorageApi;

    @Autowired
    private VehicleServiceApi vehicleServiceApi;



    @Override
    public void loadExternalData(MultipartFile file) {

        Queue<String[]> records = fileStorageApi.storeAndReadCSVFile(file);

        vehicleServiceApi.loadRecords(records);

    }

    @Override
    public void loadExternalData(String fileName) {

        Queue<String[]> records = fileStorageApi.readCSV(fileName);

        vehicleServiceApi.loadRecords(records);

    }
}
