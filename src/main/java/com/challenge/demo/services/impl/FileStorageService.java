package com.challenge.demo.services.impl;

import com.challenge.demo.services.api.FileStorageApi;
import com.challenge.demo.validators.impl.RequestValidator;
import com.opencsv.CSVReader;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.LinkedList;
import java.util.Queue;

@Service
@Slf4j
public class FileStorageService implements FileStorageApi {

    @Value("${file.uploaddir}")
    private String uploadDir;

    @Autowired
    private String executionDirectory;

    @Autowired
    private RequestValidator requestValidator;

    @Override
    public String storeFile(MultipartFile file) {

        requestValidator.isInputStringNull("fileName",file.getOriginalFilename());

        String fileName = StringUtils.cleanPath(file.getOriginalFilename());

        String result = "";

        try {

            Path filePath = Paths.get(executionDirectory, uploadDir);

            Files.createDirectories(filePath);

            Path targetLocation = filePath.resolve(fileName);

            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
            result = targetLocation.toString();

        } catch (Exception ex) {

            log.error(ex.toString());
        }

        return result;

    }

    @Override
    public Queue<String[]> readCSV(String fileName) {

        requestValidator.isInputStringNull("fileName",fileName);

        Queue<String[]> records = new LinkedList<>();

        try (
                Reader reader = Files.newBufferedReader(Paths.get(fileName));
                CSVReader csvReader = new CSVReader(reader);
        ) {

            String[] nextRecord;
            while ((nextRecord = csvReader.readNext()) != null) {

                records.add(nextRecord);
            }

        } catch (Exception ex) {
           log.error(ex.toString());
        }


        return records;
    }

    @Override
    public Queue<String[]> storeAndReadCSVFile(MultipartFile file) {

        String fileName = this.storeFile(file);
        Queue<String[]> records = this.readCSV(fileName);
        return records;
    }
}
