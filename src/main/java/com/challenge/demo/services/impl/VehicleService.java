package com.challenge.demo.services.impl;

import com.challenge.demo.components.api.VehicleMapperApi;
import com.challenge.demo.constants.RepositoryConstants;
import com.challenge.demo.entities.dtos.VehicleTraceDTO;
import com.challenge.demo.entities.models.Operator;
import com.challenge.demo.entities.models.SimplifiedVehicle;
import com.challenge.demo.entities.models.Vehicle;
import com.challenge.demo.entities.models.VehicleTrace;
import com.challenge.demo.repositories.api.VehicleRepositoryApi;
import com.challenge.demo.repositories.template.RepositoryTemplate;
import com.challenge.demo.services.api.VehicleServiceApi;
import com.challenge.demo.services.template.ServiceTemplate;
import com.challenge.demo.validators.api.RequestValidatorApi;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
public class VehicleService extends ServiceTemplate implements VehicleServiceApi {

    @Autowired
    private VehicleRepositoryApi vehicleRepositoryApi;

    @Autowired
    private RepositoryTemplate<Operator> operatorRepositoryApi;

    @Autowired
    private RepositoryTemplate<SimplifiedVehicle> simplifiedVehicleRepositoryApi;

    @Autowired
    private RepositoryTemplate<VehicleTrace> vehicleTraceRepositoryApi;

    @Value("${server.load.batch.size}")
    private int loadBatchSize;

    @Autowired
    private VehicleMapperApi vehicleMapperApi;

    @Autowired
    private RequestValidatorApi requestValidator;

    @Autowired
    private RepositoryConstants repositoryConstants;

    @Override
    public void loadRecords(Queue<String[]> recordQueue) {

        while(!recordQueue.isEmpty()) {

            List<String[]> recordsBatch = new ArrayList<>();

            int currentNumber = 0;

            while (!recordQueue.isEmpty() && currentNumber < loadBatchSize) {

                String[] currentRecord = recordQueue.remove();
                recordsBatch.add(currentRecord);
                currentNumber++;

            }

            log.info("Number Of Vehicles to Load: " + currentNumber);
            List<Vehicle> vehicleList = vehicleMapperApi.transformAll(recordsBatch);
            vehicleRepositoryApi.insertEntities(vehicleList);
        }


    }

    @Override
    public List<Operator> getOperatorsRunningByTimeInterval(String startTime, String endTime) {

        super.validateTimeInterval(startTime, endTime);

        List<String> parameters = Arrays.asList(startTime, endTime);

        String query = super.buildQuery(repositoryConstants.GET_OPERATORS_BY_TIME_INTERVAL_RUNNING,parameters );

        List<Operator> operators =  operatorRepositoryApi.executeQuery(query, Operator.class);

        return requestValidator.isEntityListEmpty(operators);
    }

    @Override
    public List<SimplifiedVehicle> getVehiclesByOperatorAndTimeInterval(String startTime, String endTime,String operator) {

        super.validateTimeInterval(startTime, endTime);

        List<String> parameters = Arrays.asList(operator, startTime, endTime);

        String query = super.buildQuery(repositoryConstants.GET_VEHICLES_BY_OPERATOR_AND_TIME_INTERVAL,parameters );

        List<SimplifiedVehicle> simplifiedVehicle =  simplifiedVehicleRepositoryApi.executeQuery(query, SimplifiedVehicle.class);

        return requestValidator.isEntityListEmpty(simplifiedVehicle);
    }

    @Override
    public List<SimplifiedVehicle> getVehiclesAtStopByOperator(String startTime, String endTime, String operator) {

        super.validateTimeInterval(startTime, endTime);

        List<String> parameters = Arrays.asList(operator, startTime, endTime);

        String queryWithStop = super.buildQuery(repositoryConstants.GET_VEHICLES_WITH_STOP_BY_OPERATOR_AND_TIME_INTERVAL,parameters );

        String queryRunning = super.buildQuery(repositoryConstants.GET_VEHICLES_RUNNING_BY_OPERATOR_AND_TIME_INTERVAL,parameters );

        List<SimplifiedVehicle> simplifiedVehicleWithStopList =  simplifiedVehicleRepositoryApi.executeQuery(queryWithStop, SimplifiedVehicle.class);

        List<SimplifiedVehicle> simplifiedVehicleRunningList =  simplifiedVehicleRepositoryApi.executeQuery(queryRunning, SimplifiedVehicle.class);

        List<SimplifiedVehicle> result = simplifiedVehicleWithStopList
                .stream()
                .filter(svws -> !simplifiedVehicleRunningList.contains(svws))
                .collect(Collectors.toList());

        return requestValidator.isEntityListEmpty(result);

    }

    @Override
    public List<VehicleTraceDTO> getTraceByVehicle(String startTime, String endTime, int vehicleId) {

        super.validateTimeInterval(startTime, endTime);

        List<String> parameters = Arrays.asList(String.valueOf(vehicleId),startTime, endTime);

        String query = super.buildQuery(repositoryConstants.GET_VEHICLE_TRACE_AND_TIME_INTERVAL,parameters );

        List<VehicleTrace> vehicleTrace =  vehicleTraceRepositoryApi.executeQuery(query, VehicleTrace.class);

        List<VehicleTraceDTO> vehicleTraceDTOList = this.mapToVehicleTraceDTO(vehicleTrace);

        return requestValidator.isEntityListEmpty(vehicleTraceDTOList);
    }

    private List<VehicleTraceDTO> mapToVehicleTraceDTO (List<VehicleTrace> vehicleTrace){

        List<VehicleTraceDTO> result = vehicleTrace.stream().map(vt -> {

            VehicleTraceDTO vehicleTraceDTO = new VehicleTraceDTO();
            vehicleTraceDTO.setLatitude(vt.getLatitude());
            vehicleTraceDTO.setLongitude(vt.getLongitude());
            vehicleTraceDTO.setTime(new Date(vt.getTime().toEpochMilli()));
            return vehicleTraceDTO;

        })
        .collect(Collectors.toList());

        return result;
    }
}
