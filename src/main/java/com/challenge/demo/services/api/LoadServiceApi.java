package com.challenge.demo.services.api;

import org.springframework.web.multipart.MultipartFile;

public interface LoadServiceApi {

    void loadExternalData(MultipartFile file);

    void loadExternalData(String fileName);
}
