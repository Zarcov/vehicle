package com.challenge.demo.services.api;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Queue;

public interface FileStorageApi {

    String storeFile(MultipartFile file);

    Queue<String[]> readCSV(String filename);

    Queue<String[]> storeAndReadCSVFile(MultipartFile file);

}
