package com.challenge.demo.services.api;

import com.challenge.demo.entities.dtos.VehicleTraceDTO;
import com.challenge.demo.entities.models.Operator;
import com.challenge.demo.entities.models.SimplifiedVehicle;

import java.util.List;
import java.util.Queue;

public interface VehicleServiceApi {

    void loadRecords(Queue<String[]> records);

    List<Operator> getOperatorsRunningByTimeInterval(String startTime, String endTime);

    List<SimplifiedVehicle> getVehiclesByOperatorAndTimeInterval(String startTime, String endTime, String operator);

    List<SimplifiedVehicle> getVehiclesAtStopByOperator(String startTime, String endTime, String operator);

    List<VehicleTraceDTO> getTraceByVehicle(String startTime, String endTime, int vehicleId);

}
