package com.challenge.demo.entities.models;

import lombok.*;
import org.influxdb.annotation.Column;
import org.influxdb.annotation.Measurement;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
@Measurement(name = "Vehicle")
public class Operator {

    @Column(name = "operator",tag = true)
    private String operator;
}
