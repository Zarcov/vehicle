package com.challenge.demo.entities.models;

import lombok.*;
import org.influxdb.annotation.Column;
import org.influxdb.annotation.Measurement;

import java.time.Instant;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Measurement(name = "Vehicle")
@Builder
public class Vehicle {

        @Column(name = "time")
        private Instant time;

        @Column(name = "lineId")
        private Integer lineId;

        @Column(name = "direction")
        private Integer direction;

        @Column(name = "jouneyPatternId")
        private Integer jouneyPatternId;

        @Column(name = "timeFrame")
        private String timeFrame;

        @Column(name = "vehicleJourneyId")
        private Integer vehicleJourneyId;

        @Column(name = "operator",tag = true)
        private String operator;

        @Column(name = "congestion")
        private Integer congestion;

        @Column(name = "longitude")
        private Double longitude;

        @Column(name = "latitude")
        private Double latitude;

        @Column(name = "delay")
        private Integer delay;

        @Column(name = "blockId")
        private Integer blockId;

        @Column(name = "vehicleId")
        private Integer vehicleId;

        @Column(name = "stopId")
        private Integer stopId;

        @Column(name = "stop")
        private boolean stop;

        @Override
        public boolean equals(Object o){
                if(o instanceof Vehicle){

                        Vehicle v = (Vehicle) o;

                        return this.getTime().equals(v.getTime()) &&
                                this.getLineId() == v.getLineId() &&
                                this.getDirection() == v.getDirection() &&
                                this.getJouneyPatternId() == v.getJouneyPatternId() &&
                                this.getOperator() == v.getOperator() &&
                                this.getCongestion() == v.getCongestion() &&
                                Double.compare(this.getLongitude(), v.getLongitude()) == 0 &&
                                Double.compare(this.getLatitude(), v.getLatitude()) == 0 &&
                                this.getDelay() == v.getDelay() &&
                                this.getBlockId() == v.getBlockId() &&
                                this.getVehicleId() == v.getVehicleId() &&
                                this.getStopId() == v.getStopId() &&
                                this.isStop() == v.isStop();
                }
                return false;
        }
}

