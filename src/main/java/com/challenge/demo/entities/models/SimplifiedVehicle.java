package com.challenge.demo.entities.models;

import lombok.*;
import org.influxdb.annotation.Column;
import org.influxdb.annotation.Measurement;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
@Measurement(name = "Vehicle")
public class SimplifiedVehicle {

    @Column(name = "vehicleId")
    private Integer vehicleId;

    @Override
    public boolean equals(Object o){
        if(o instanceof SimplifiedVehicle){
            int vehicleId = ((SimplifiedVehicle) o).vehicleId;
            return this.vehicleId == vehicleId;
        }
        return false;
    }
}
