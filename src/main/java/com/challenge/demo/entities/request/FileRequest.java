package com.challenge.demo.entities.request;


import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public class FileRequest {

    private String fileName;
}
