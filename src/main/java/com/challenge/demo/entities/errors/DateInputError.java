package com.challenge.demo.entities.errors;

import lombok.*;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class DateInputError extends RuntimeException {

    public DateInputError(String errorMessage){

        super(errorMessage);
    }
}
