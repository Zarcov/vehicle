package com.challenge.demo.entities.errors;


import lombok.*;
import org.springframework.http.HttpStatus;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public class ApiError {

    private String name;
    private HttpStatus status ;
    private String errorMessage;

}
