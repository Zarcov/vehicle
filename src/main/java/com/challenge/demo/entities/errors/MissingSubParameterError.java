package com.challenge.demo.entities.errors;


import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
@Getter
public class MissingSubParameterError extends RuntimeException {

    private String parameterName;

    public MissingSubParameterError(String parameterName){

        super();

        this.parameterName = parameterName;

    }
}
