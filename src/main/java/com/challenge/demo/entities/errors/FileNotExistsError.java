package com.challenge.demo.entities.errors;


import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
@Getter
public class FileNotExistsError extends RuntimeException {

    private String fileName;

    public FileNotExistsError(String fileName){

        super();

        this.fileName = fileName;

    }
}
