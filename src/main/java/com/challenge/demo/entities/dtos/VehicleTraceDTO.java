package com.challenge.demo.entities.dtos;

import lombok.*;
import org.influxdb.annotation.Column;

import java.time.Instant;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public class VehicleTraceDTO {

    private Date time;

    private Double latitude;

    private Double longitude;
}
