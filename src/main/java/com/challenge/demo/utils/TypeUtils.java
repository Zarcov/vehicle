package com.challenge.demo.utils;

import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;

@Component
public class TypeUtils {

    public boolean isValidFormat(String format, String value) {
        Date date = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            date = sdf.parse(value);
            if (!value.equals(sdf.format(date))) {
                date = null;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return date != null;
    }

    public Instant parseDateToInstant(String rawDate){

        Long microSeconds = Long.parseLong(rawDate);

        Instant timestamp = Instant.ofEpochMilli(microSeconds);

        return timestamp;
    }

    public Integer parseStringToInt(String rawString){

        Integer result;

        try {

            result = Integer.parseInt(rawString);

        }catch(Exception ex){

            return null;
        }

        return result;

    }

    public Boolean parseStringToBoolean(String rawString){

        Boolean result;

        try {

            result = rawString != null && (Boolean.parseBoolean(rawString) || rawString.equals("1"));

        }catch(Exception ex){

            return null;
        }

        return result;

    }

    public Double parseStringToDouble(String rawString){

        Double result;

        try {

            result = Double.parseDouble(rawString);

        }catch(Exception ex){

            return null;
        }

        return result;
    }
}
