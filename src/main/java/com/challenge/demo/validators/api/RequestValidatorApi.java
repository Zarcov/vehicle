package com.challenge.demo.validators.api;

import java.util.List;

public interface RequestValidatorApi {

    void doesFileExist(String fileName);

    void isInputStringNull(String parameterName, String parameterValue);

    <T> List<T> isEntityListEmpty (List<T> entityList);

    void isTimeIntervalValid(String startTime, String endTime);

}
