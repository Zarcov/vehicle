package com.challenge.demo.validators.impl;

import com.challenge.demo.constants.ApplicationConstants;
import com.challenge.demo.entities.errors.DateInputError;
import com.challenge.demo.entities.errors.FileNotExistsError;
import com.challenge.demo.entities.errors.MissingSubParameterError;
import com.challenge.demo.entities.errors.NotFoundError;
import com.challenge.demo.utils.TypeUtils;
import com.challenge.demo.validators.api.RequestValidatorApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.List;

@Component
public class RequestValidator implements RequestValidatorApi {

    @Autowired
    private TypeUtils typeUtils;

    @Autowired
    private ApplicationConstants applicationConstants;

    @Override
    public void isInputStringNull(String parameterName, String parameterValue){

        if(parameterValue == null || parameterValue.isEmpty()) {

            throw new MissingSubParameterError(parameterName);

        };

    }

    @Override
    public void doesFileExist(String fileName){

        File f = new File(fileName);

        if(!f.isFile()) {

            throw new FileNotExistsError(fileName);

        };

    }

    @Override
    public <T> List<T> isEntityListEmpty (List<T> entityList){

        if(entityList == null || entityList.size() == 0) {throw new NotFoundError();};

        return entityList;
    }

    @Override
    public void isTimeIntervalValid(String startTime, String endTime){

        boolean isValidStartTime = typeUtils.isValidFormat(applicationConstants.TIME_FORMAT, startTime);
        boolean isValidEndTime = typeUtils.isValidFormat(applicationConstants.TIME_FORMAT, endTime);

        if(!isValidStartTime || !isValidEndTime){

            String errorMessage = String.format(applicationConstants.BAD_REQUEST_DATE_FORMAT_ERROR_MESSAGE,applicationConstants.TIME_FORMAT, startTime,endTime);
            throw new DateInputError(errorMessage);
        };

    }
}
