package com.challenge.demo.config;

import lombok.extern.slf4j.Slf4j;
import org.influxdb.InfluxDB;
import org.influxdb.InfluxDBFactory;
import org.influxdb.dto.Pong;
import org.influxdb.dto.Query;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.TimeUnit;

@Configuration
@Slf4j
public class DatabaseConfig {

    @Value("${server.database.url}")
    private String url;

    @Value("${server.database.username}")
    private String username;

    @Value("${server.database.password}")
    private String password;

    @Value("${server.database.name}")
    private String dbName;

    @Bean(name="dbContext")
    public InfluxDB getDatabaseContext () {

        InfluxDB influxDB = InfluxDBFactory.connect(url, username, password);

        influxDB.query(new Query("CREATE DATABASE " + dbName,""));

        influxDB.enableBatch(100, 200, TimeUnit.MILLISECONDS);
        influxDB.setRetentionPolicy("defaultPolicy");
        influxDB.setDatabase(dbName);

        Pong response = influxDB.ping();
        if (response.getVersion().equalsIgnoreCase("unknown")) {

            log.error("Error pinging server.");

        }

        return influxDB;

    }
}
