package com.challenge.demo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FileConfig {

    @Bean(name="executionDirectory")
    public String getExecutionDirectory(){

        return System.getProperty("user.dir");
    }

}



