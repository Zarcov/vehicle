package com.challenge.demo.components;

import com.challenge.demo.components.impl.VehicleMapper;
import com.challenge.demo.entities.models.Vehicle;
import com.challenge.demo.utils.TypeUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.Instant;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class VehicleMapperTests {

    @InjectMocks
    private VehicleMapper vehicleMapper;

    @Mock
    private TypeUtils typeUtils;

    private Vehicle mockVehicle;

    @Before
    public void init() {

        Long microSecond = 1358502215000000000L;

        Instant timestamp = Instant.ofEpochMilli(microSecond);
        mockVehicle = Vehicle.builder()
                .time(timestamp)
                .lineId(1)
                .direction(1)
                .jouneyPatternId(1)
                .timeFrame("2013-01-17")
                .vehicleJourneyId(1)
                .operator("HN")
                .congestion(1)
                .longitude(1.0)
                .latitude(1.0)
                .delay(1)
                .blockId(1)
                .vehicleId(1)
                .stopId(1)
                .stop(true)
                .build();

        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testTransformVehicle() {

        String[] record = new String[]{"1358502215000000000","1","1","1","2013-01-17","1","HN","1","1.0","1.0","1","1","1","1","1"};
        when(typeUtils.parseStringToInt(any(String.class))).thenReturn(1);
        when(typeUtils.parseStringToDouble(any(String.class))).thenReturn(1.0);
        when(typeUtils.parseStringToBoolean(any(String.class))).thenReturn(true);
        when(typeUtils.parseDateToInstant(any(String.class))).thenReturn(mockVehicle.getTime());

        Vehicle vehicle = vehicleMapper.transform(record);

        assertEquals(mockVehicle, vehicle );
    }
    @Test
    public void testTransformVehicleEmptyInput() {

        String[] record = new String[]{};

        Vehicle vehicle = vehicleMapper.transform(record);

        assertEquals(null, vehicle );
    }

    @Test
    public void testTransformVehicleIncorrectSizeInput() {

        String[] record = new String[]{"1358467201000000","1","1","1","2013-01-17","1","HN","1","1.0","1.0","1","1","1","1"};

        Vehicle vehicle = vehicleMapper.transform(record);

        assertEquals(null, vehicle );
    }
}
