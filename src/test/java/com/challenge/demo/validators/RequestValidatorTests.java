package com.challenge.demo.validators;

import com.challenge.demo.constants.ApplicationConstants;
import com.challenge.demo.entities.errors.DateInputError;
import com.challenge.demo.entities.errors.MissingSubParameterError;
import com.challenge.demo.entities.errors.NotFoundError;
import com.challenge.demo.utils.TypeUtils;
import com.challenge.demo.validators.impl.RequestValidator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RequestValidatorTests {

    @InjectMocks
    private RequestValidator requestValidator;

    @Mock
    private TypeUtils typeUtils;

    @Mock
    private ApplicationConstants applicationConstants;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test(expected = MissingSubParameterError.class)
    public void checkIfRequestValidatorThrowsErrorIfInputIsNull() {

        requestValidator.isInputStringNull("abc",null);

    }

    @Test(expected = MissingSubParameterError.class)
    public void checkIfRequestValidatorThrowsErrorIfInputIsEmpty() {

        requestValidator.isInputStringNull("abc","");

    }

    @Test()
    public void checkIfIsEntityListEmptyPassesIfInputIsOK() {

        requestValidator.isEntityListEmpty(Arrays.asList("abc"));

    }

    @Test(expected = NotFoundError.class)
    public void checkIfIsEntityListEmptyPassesIfInputIsNull() {

        List<String> abc = new ArrayList<>();
        requestValidator.isEntityListEmpty(abc);

    }

    @Test(expected = NotFoundError.class)
    public void checkIfIsEntityListEmptyPassesIfInputIsEmpty() {

        requestValidator.isEntityListEmpty(Arrays.asList());

    }

    @Test()
    public void checkIfRequestValidatorPassesIfInputIsOK() {

        requestValidator.isInputStringNull("abc","abc");

    }


    @Test()
    public void checkIfIisTimeIntervalValidThrowsErrorIfDatesAreValid() {

        String startTime = "";
        String endTime = "";
        applicationConstants.TIME_FORMAT="";

        when(typeUtils.isValidFormat(any(String.class),any(String.class))).thenReturn(true);

        requestValidator.isTimeIntervalValid(startTime,endTime );

    }

    @Test(expected = DateInputError.class)
    public void checkIfIisTimeIntervalValidThrowsErrorIfDatesAreInvalid() {

        String startTime = "";
        String endTime = "";
        applicationConstants.TIME_FORMAT="";
        applicationConstants.BAD_REQUEST_DATE_FORMAT_ERROR_MESSAGE = "";
        when(typeUtils.isValidFormat(any(String.class),any(String.class))).thenReturn(false);

        requestValidator.isTimeIntervalValid(startTime,endTime );

    }
}
