package com.challenge.demo.validators;

import com.challenge.demo.utils.TypeUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class TypeUtilsTests {

    @InjectMocks
    private TypeUtils typeUtils;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testIsValidFormatForEmptyDateAndFormat() {

        boolean isValid = typeUtils.isValidFormat("", "");

        assertEquals(false, isValid );
    }

    @Test
    public void testIsValidFormatForEmptyDate() {

        boolean isValid = typeUtils.isValidFormat("yyyy-MM-dd", "");

        assertEquals(false, isValid );
    }

    @Test
    public void testIsValidFormatForCorrectInputs() {

        boolean isValid = typeUtils.isValidFormat("yyyy-MM-dd", "2019-03-03");

        assertEquals(true, isValid );
    }

    @Test
    public void testIsValidFormatForNullInputs() {

        boolean isValid = typeUtils.isValidFormat(null, null);

        assertEquals(false, isValid );
    }

    @Test
    public void testParseStringToBooleanForInvalidInputs() {

        boolean isValidNull = typeUtils.parseStringToBoolean(null);
        boolean isValidEmpty = typeUtils.parseStringToBoolean("");
        boolean isValidABC = typeUtils.parseStringToBoolean("ABC");

        assertEquals(false, isValidNull );
        assertEquals(false, isValidEmpty );
        assertEquals(false, isValidABC );
    }


    @Test
    public void testParseStringToBooleanForValidInputs() {

        boolean isValid1 = typeUtils.parseStringToBoolean("1");
        boolean isValidTrue = typeUtils.parseStringToBoolean("true");

        assertEquals(true, isValid1 );
        assertEquals(true, isValidTrue );
    }

}
