swagger: '2.0'
info:
  version: 1.0.0
  title: Vehicle Challenge API
  description: 'API To load end expose Vehicle IoT Data'
schemes:
  - https
paths:
  '/api/vehicles/fileupload':
    post:
      tags:
        - LoadServices
      summary: File Uploads a CSV file with Vehicle measurements
      description: File Uploads a CSV file with Vehicle measurements
      operationId: LoadExternalDataFileUpload
      consumes:
        - multipart/form-data
      parameters:
        - in: formData
          name: upfile
          type: file
          description: The file to upload (.csv file).
      responses:
        '200':
          description: successful operation
        '500':
          description: Internal server error
          schema:
            $ref: '#/definitions/Error'
  '/api/vehicles/nonfileupload':
    post:
      tags:
        - LoadServices
      summary: Upload a CSV file with Vehicle measurements that is specified in RequestBody
      description: Upload CSV file through POST Request Body with Vehicle measurements
      operationId: LoadExternalDataNonFileUpload
      consumes:
        - application/json
      parameters:
        - in: body
          name: fileRequest
          schema:
            $ref: '#/definitions/FileRequest'
      responses:
        '200':
          description: successful operation
        '500':
          description: Internal server error
          schema:
            $ref: '#/definitions/Error'
  '/api/operators/running':
    get:
      tags:
        - VehicleServices
      summary: getOperatorsRunningByTimeInterval
      description: Get All Operators Running in an Time Interval. Running Operators have at least 1 record not Stopped (Stop=false) in the Time Interval
      operationId: getOperatorsRunningByTimeInterval
      parameters:
        - name: startTime
          in: query
          required: true
          type: string
          description: The Start time interval to perform the service. The date follows ISO8601 Date Format (yyyy-MM-dd HH:mm:ss.SSS). Any Date in another format will be considered a Bad Request.
        - name: endTime
          in: query
          required: true
          type: string
          description: The End time interval to perform the service. The date follows ISO8601 Date Format (yyyy-MM-dd HH:mm:ss.SSS). Any Date in another format will be considered a Bad Request.
      consumes:
        - application/json
      produces:
        - application/json
      responses:
        '200':
          description: successful operation
          schema:
            $ref: '#/definitions/Operator'
        '400':
          description: Bad Request Error. You can get BadRequest if => 1. StartTime/EndTime are not present in the request. 2. StartTime/Endtime don't follow the API Date Format.
          schema:
            $ref: '#/definitions/Error'
        '404':
          description: NotFound error
          schema:
            $ref: '#/definitions/Error'
        '500':
          description: Internal server error
          schema:
            $ref: '#/definitions/Error'
  '/api/vehicles/operator/{operator}':
    get:
      tags:
        - VehicleServices
      summary: getVehiclesByOperatorAndTimeInterval
      description: Get All Vehicles by Operators and Time Interval
      operationId: getVehiclesByOperatorAndTimeInterval
      parameters:
        - in: path
          name: operator
          required: true
          description: Name of the operator (Fleet)
          type: string
        - name: startTime
          in: query
          required: true
          type: string
          description: The Start time interval to perform the service. The date follows ISO8601 Date Format (yyyy-MM-dd HH:mm:ss.SSS). Any Date in another format will be considered a Bad Request.
        - name: endTime
          in: query
          required: true
          type: string
          description: The End time interval to perform the service. The date follows ISO8601 Date Format (yyyy-MM-dd HH:mm:ss.SSS). Any Date in another format will be considered a Bad Request.
      consumes:
        - application/json
      produces:
        - application/json
      responses:
        '200':
          description: successful operation
          schema:
            $ref: '#/definitions/SimplifiedVehicle'
        '500':
          description: Internal server error
          schema:
            $ref: '#/definitions/Error'
  '/api/vehicles/stop/operator/{operator}':
    get:
      tags:
        - VehicleServices
      summary: GetVehiclesAtStopByOperatorAndTimeInterval
      description: >-
        Gets all Vehicles at Stop by Operator and Time Interval. Stopped Vehicles have all records saying they are Stopped (Stop=true) in the Time Interval
      operationId: GetVehiclesAtStopByOperatorAndTimeInterval
      consumes:
        - application/json
      produces:
        - application/json
      parameters:
        - in: path
          name: operator
          required: true
          description: Name of the operator (Fleet)
          type: string
        - name: startTime
          in: query
          required: true
          type: string
          description: The Start time interval to perform the service. The date follows ISO8601 Date Format (yyyy-MM-dd HH:mm:ss.SSS). Any Date in another format will be considered a Bad Request.
        - name: endTime
          in: query
          required: true
          type: string
          description: The End time interval to perform the service. The date follows ISO8601 Date Format (yyyy-MM-dd HH:mm:ss.SSS). Any Date in another format will be considered a Bad Request.
      responses:
        '200':
          description: successful operation
          schema:
            $ref: '#/definitions/SimplifiedVehicle'
        '404':
          description: Not Found. The Endpoint can return 2 types of notFound Errors => Type 1. The Organisation does not exist Type 2. The Organisation does not have any Relationships.
          schema:
            $ref: '#/definitions/Error'
        '400':
          description: Bad Request. If the user uses a pageNumber, it must a positive Integer. Otherwise a BadRequestPageNumber is thrown. 
          schema:
            $ref: '#/definitions/Error'
        '500':
          description: Internal server error
          schema:
            $ref: '#/definitions/Error'
  '/api/trace/vehicle/{vehicleId}':
    get:
      tags:
        - VehicleServices
      summary: GetVehiclesAtStopByOperatorAndTimeInterval
      description: >-
        Get all Trace (Time, Latitude, Longitude) of a certain Vehicle in a Time Interval
      operationId: GetTraceByVehicleAndTimeInterval
      consumes:
        - application/json
      produces:
        - application/json
      parameters:
        - in: path
          name: vehicleId
          required: true
          description: Unique Identifier of a Vehicle
          type: string
        - name: startTime
          in: query
          required: true
          type: string
          description: The Start time interval to perform the service. The date follows ISO8601 Date Format (yyyy-MM-dd HH:mm:ss.SSS). Any Date in another format will be considered a Bad Request.
        - name: endTime
          in: query
          required: true
          type: string
          description: The End time interval to perform the service. The date follows ISO8601 Date Format (yyyy-MM-dd HH:mm:ss.SSS). Any Date in another format will be considered a Bad Request.
      responses:
        '200':
          description: successful operation
          schema:
            $ref: '#/definitions/VehicleTrace'
        '404':
          description: Not Found. The Endpoint can return 2 types of notFound Errors => Type 1. The Organisation does not exist Type 2. The Organisation does not have any Relationships.
          schema:
            $ref: '#/definitions/Error'
        '400':
          description: Bad Request. If the user uses a pageNumber, it must a positive Integer. Otherwise a BadRequestPageNumber is thrown. 
          schema:
            $ref: '#/definitions/Error'
        '500':
          description: Internal server error
          schema:
            $ref: '#/definitions/Error'
definitions:
  Operator:
    type: object
    properties:
      fileName:
        type: string
        description: Full file path of the CSV file to upload = C:\\Users\\pmfranco\\Downloads\\sir010113-310113\\siri.20130101.csv\\siri.20130101.csv
  FileRequest:
    type: object
    properties:
      fileName:
        type: string
  SimplifiedVehicle:
    type: object
    properties:
      vehicleId:
        type: integer
  VehicleTrace:
    type: object
    properties:
      time:
        type: string
        description: Time of a specific Data Point (DateFormat=> yyyy-MM-dd HH:mm:ss.SSS)
      latitude:
        type: string
        description: Vehicle Latitude at a specific time point 
      longitude:
        type: string
        description: Name of the Organisation
  Error:
    type: object
    properties:
      name : 
        type: string
        description: "The name of the error (Subtype)"
      errorMessage:
        type: string
        description: Full error from the API
      status:
        type: string
        description: HTTPStatus Code of the Error
