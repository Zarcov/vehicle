# Vehicle-Challenge

Hi there! Here is my solution proposal for Daimler Trucks and Buses's Vehicle Challenge.

## Built With

The project was developed in Spring Boot (Java Web Framework) and InfluxDB (Time Series database).

- [Java](https://www.java.com/pt_BR/download/) - Programming Language
- [Maven](https://maven.apache.org/) - Dependency Management
- [Spring Boot](https://spring.io/projects/spring-boot) - Web framework for Java REST Services
- [InfluxDB](https://www.influxdata.com/) - Open Source Time Series Database
- [Docker](https://www.docker.com/) - Application Container Platform
- [Junit/Mockito](https://junit.org/junit5/) - Unit Testing
- [Postman](https://www.getpostman.com/) - API Testing
- [Swagger](https://swagger.io/) - API Documentation

## Getting Started

These instructions will allow to get a copy of the project up and running on your local machine for development.

### Prerequisites

The project has the following requesites:

- [Java 8](https://www.oracle.com/technetwork/pt/java/javase/downloads/jdk8-downloads-2133151.html)
- [Maven](https://www.baeldung.com/install-maven-on-windows-linux-mac)
- [Docker](https://docs.docker.com/docker-for-windows/install/) 

In order to install the software, the Java and Docker dependencies must be installed.

Maven is optional if you want to run the application through cli. To run the application in your favourite IDE, Maven configuration is not necessary.

Java is necessary to spin up the Spring boot application while Docker is necessary for InfluxDB image management.

You can verify that the 3 requesites have been installed & setup properly by running the following commands in the command prompt:

java -version
mvn -v
docker -v

### Database Setup with Docker

Step 0 - Download Influxdb Docker image

```
docker pull influxdb
```

Step 1 - Run InfluxDB with PortMapping (In the example below 8087 will be the exposed Port to the host.)

```
docker run -p 8087:8086 influxdb
```

Step 2 - Check Docker Image is Running (in the result, an image called 'influxdb' should be listed)

```
docker container ls
```

Step 3 - Connect to InfluxCLI console to create Admin User

```
docker exec -it influxdb influx
```
 
Step 4 - Create Admin User with Username and Password of your choosing

```
CREATE USER admin WITH admin WITH ALL PRIVILEGES
```

Step 5 - Test if Influxdb is connectable from host

Call 'http://localhost:8086/query?' in a browser of your choosing. You should receive the following message: 

```
{"error":"missing required parameter \"q\""}
```

### Software Installation (Java application)

A step by step series of examples that tell you how to get a development env running

Step 0 - Clone the repository with the below URL

```
https://gitlab.com/Zarcov/vehicle.git
```

Step 1 - Open cmd and cd to the root directory of the project (example directory below)

```
cd C:\Users\<user>\Documents\Repositories\vehicle

```

If you don't want to run the application through cli and only run it through your IDE (e.g. Intellij), **Steps 2 and 3 are not required**.

You just need to configure the following properties in application.properties file in resources folder.

- server.port - The HTTP port that the application will use
- server.database.url - The URL of the InfluxDB (defined in the database setup => port 8087 in the instructions above)
- server.database.username - Username of the user created in the database setup (defined in the database setup => admin in the instructions above)
- server.database.password - Password of the user created in the database setup (defined in the database setup => admin in the instructions above)


Step 2 - Install Project dependencies in Root Directory with Maven

```
mvn install
```

Step 3 - Start application. You need to specify 3 arguments on execution:

- server.port - The HTTP port that the application will use
- server.database.url - The URL of the InfluxDB (defined in the database setup => port 8087 in the instructions above)
- server.database.username - Username of the user created in the database setup (defined in the database setup => admin in the instructions above)
- server.database.password - Password of the user created in the database setup (defined in the database setup => admin in the instructions above)

Example of Full Command to start the application with Maven:
```
mvn spring-boot:run -Dspring-boot.run.arguments="--server.port=8085,--server.database.url=http://localhost:8087,--server.database.username=admin,--server.database.password=admin"

```

Step 4 - Smoke Test application in Browser

```
http://localhost:8085/api/operators/running?startTime=2013-01-01 18:05:03.760&endTime=2013-01-01 20:05:03.760
```

You should receive the following Response (404):

```
{
    "name": "NotFound",
    "status": "NOT_FOUND",
    "errorMessage": "No Result was found"
}
```


## DataLoader

There are 2 strategies to load CVS files through the API (the endpoints are in the Postman Collection in the docs folder).

**Strategy 1 'FileUpload' API Endpoint**

You upload the CSV file by using the FileUpload API Endpoint (POST Request).

You can use Postman and 'DataLoader - FileUpload' in the Postman API Collection in docs folder to use this strategy.

**Strategy 2 'NonFileUpload' API Endpoint**
 
You import the CSV file by using the NonFileUpload API Endpoint (POST Request).

The CSV file is not uploaded but the full file path is sent in the POST request body.

You can use Postman and 'DataLoader - NonFileUpload' in the Postman API Collection in docs folder to use this strategy.

**Strategy 3 CURL Command**

You can execute the NonFileUpload Endpoint through cmd directly without Postman.

```
curl -X POST "http://localhost:8085/api/vehicles/nonfileupload" -H "accept: application/json" -H "Content-Type: application/json" -d "{ \"fileName\": \"C:\\Users\\pmfranco\\Downloads\\sir010113-310113\\siri.20130118.csv\\siri.20130118.csv\"}"
```

In the above example you need to choose the location of the CSV file and the URL of the API.

## Running the tests

All project Unit tests can be run with the following command in the root directory after installation:

```
mvn test
```
A Postman Collection is provided to do API Testing (docs folder).

## Documentation (docs folder)

- [Swagger (API Documentation)] - You can import the yaml file in https://editor.swagger.io/
- [Postman Collection] - 'Vehicle-Challenge.json' file

## Authors

- **Pedro Franco** - (https://www.linkedin.com/in/pmgfranco/)

## Acknowledgments

- Daimler Trucks and Buses for the opportunity to participate in the challenge
